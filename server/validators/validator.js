const { Validator } = require("node-input-validator");
const { constants } = require("../helpers/constants");
const resHndlr = require('../helpers/responseHandler')


exports.loginValidator = async (req, res, next) => {
  const validator = new Validator(req.body, {
    username: "required",
    password: "required",
  });

  let check = await validator.check();
  if (!check)
    return resHndlr.apiResponse(res, false, constants.USER_VALIDATION, 400, {}, "");
  else
    next();
};


exports.itemValidator = async (req, res, next) => {
  const validator = new Validator(req.body, {
    itemName: "required",
    itemStatus: `required|in:${constants.state.join(",")}`,
  });

  let check = await validator.check();
  if (!check)
    return resHndlr.apiResponse(res, false, constants.ITEM_VALIDATION, 400, {}, "");
  else
    next();
};