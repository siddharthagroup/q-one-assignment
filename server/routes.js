const { itemRouter } = require("./routes/itemRoutes");
const { usrRouter } = require("./routes/userRoutes");
const router = require("express").Router();
const { verifyAuth } = require("./auth/passportAuth");

router.use("/users", usrRouter);
router.use("/items", verifyAuth, itemRouter);

module.exports = { router };
