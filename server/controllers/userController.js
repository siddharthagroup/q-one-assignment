const { checkUser, createUser } = require('../services/userService')
const { authToken } = require('../auth/jwtAuth')
const { constants } = require('../helpers/constants')


exports.createUser = async function (params) {
    try {
        let getUser = await checkUser({ username: params.username })
        if (getUser) {
            throw { err: constants.USER_EXISTS }
        } else {
            let user = await createUser(params)
            return user;
        }
    } catch (error) {
        throw error
    }
}


exports.login = async function (params) {
    try {
        let getUser = await checkUser({ username: params.username })
        if (getUser) {
            if (getUser.password == params.password) {
                const token = authToken({
                    _id: getUser._id,
                    username: getUser.username
                })
                return token;
            } else {
                throw { err: constants.INVALID_PASS }
            }
        } else {
            throw { err: constants.INVALID_USER }
        }
    } catch (error) {
        throw error
    }
}