const { ObjectId } = require('mongodb')
const { createItem, getItemById, getItemList, getItemByName, updateItem, deleteItemById, checkItem } = require('../services/itemService')
const { constants } = require('../helpers/constants')


exports.addItem = async function (params) {
    try {
        let checkItemExists = await checkItem({ itemName: params.itemName })
        if (!checkItemExists) {
            let item = await createItem({ itemName: params.itemName, itemStatus: params.itemStatus, addedBy: params.username, dateAdded: new Date() })
            return item;
        } else {
            throw { err: constants.ITEM_ALREADY_EXISTS }
        }
    } catch (error) {
        throw error
    }
}


exports.getItemList = async function () {
    try {
        let itemList = await getItemList();
        return itemList;
    } catch (error) {
        throw new Error(error)
    }
}

exports.getItemById = async function (params) {
    try {
        let item = await getItemById({ itemId: params.itemId })
        if (!item)
            throw { err: constants.NO_ITEM_FOUND }
        else
            return item;
    } catch (error) {
        throw error
    }
}

exports.updateItemById = async function (params) {
    try {
        let item = await getItemById({ itemId: params.itemId })
        if (!item)
            throw { err: constants.NO_ITEM_FOUND }
        else {
            let checkItem = await getItemByName({ itemName: params.itemName })
            if (checkItem)
                throw { err: constants.DUPLICATE_ITEM }
            else {
                let updatedItem = await updateItem({ _id: new ObjectId(params.itemId) },
                    {
                        $set: { itemName: params.itemName, itemStatus: params.itemStatus },
                    }
                );
                return updatedItem;
            }
        }
    } catch (error) {
        throw error
    }
}


exports.deleteItemById = async function (params) {
    try {
        let item = await getItemById({ itemId: params.itemId })
        if (!item)
            throw { err: constants.NO_ITEM_FOUND }
        let deleteItem = await deleteItemById({ itemId: params.itemId })
        return deleteItem;
    } catch (error) {
        throw error
    }
}