const UserController = require('../controllers/userController')
const validators = require('../validators/validator')
const usrRouter = require('express').Router()
const resHndlr = require('../helpers/responseHandler')
const { constants } = require('../helpers/constants')



usrRouter.route("/createUser")
    .post([validators.loginValidator], function (req, res) {
        let { username, password } = req.body;
        UserController.createUser({ username, password })
            .then(function (result) {
                resHndlr.apiResponse(res, true, constants.USER_CREATED, 200, result, {})
            }).catch(function (err) {
                resHndlr.apiResponse(res, false, constants.ERROR, 500, err, "");
            })
    });

usrRouter.route("/login")
    .post([validators.loginValidator], function (req, res) {
        let { username, password } = req.body;
        UserController.login({ username, password })
            .then(function (result) {
                resHndlr.apiResponse(res, true, "Successfully Logged In", 200, result, {})
            }).catch(function (err) {
                resHndlr.apiResponse(res, false, constants.ERROR, 500, err, "");
            })
    });



module.exports = { usrRouter };    