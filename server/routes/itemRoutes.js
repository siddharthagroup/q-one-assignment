const ItemController = require('../controllers/itemController')
const validators = require('../validators/validator')
const itemRouter = require('express').Router()
const resHndlr = require('../helpers/responseHandler')
const { constants } = require('../helpers/constants')

itemRouter.route("/addItem")
    .post([validators.itemValidator], function (req, res) {
        let { username } = req.user;
        let { itemName, itemStatus } = req.body;
        ItemController.addItem({ username, itemName, itemStatus })
            .then(function (result) {
                resHndlr.apiResponse(res, true, constants.ITEM_ADDED, 200, result, {})
            }).catch(function (err) {
                resHndlr.apiResponse(res, false, constants.ERROR, 500, err, "");
            })
    });


itemRouter.route("/getItemList")
    .get([], function (req, res) {
        ItemController.getItemList()
            .then(function (result) {
                resHndlr.apiResponse(res, true, constants.ITEM_FOUND, 200, result, {})
            }).catch(function (err) {
                resHndlr.apiResponse(res, false, constants.ERROR, 500, err, "");
            })
    });


itemRouter.route("/getItemById")
    .get([], function (req, res) {
        let { itemId } = req.query;
        ItemController.getItemById({ itemId })
            .then(function (result) {
                resHndlr.apiResponse(res, true, constants.ITEM_FOUND, 200, result, {})
            }).catch(function (err) {
                resHndlr.apiResponse(res, false, constants.ERROR, 500, err, "");
            })
    });


itemRouter.route("/updateItemById/:itemId")
    .put([validators.itemValidator], function (req, res) {
        let { itemId } = req.params;
        let { itemName, itemStatus } = req.body;
        ItemController.updateItemById({ itemId, itemName, itemStatus })
            .then(function (result) {
                resHndlr.apiResponse(res, true, constants.ITEM_UPDATED, 200, result, {})
            }).catch(function (err) {
                resHndlr.apiResponse(res, false, constants.ERROR, 500, err, "");
            })
    });


itemRouter.route("/deleteItemById/:itemId")
    .delete([], function (req, res) {
        let { itemId } = req.params;
        ItemController.deleteItemById({ itemId })
            .then(function (result) {
                resHndlr.apiResponse(res, true, constants.ITEM_DELETED, 200, result, {})
            }).catch(function (err) {
                resHndlr.apiResponse(res, false, constants.ERROR, 500, err, "");
            })
    });



module.exports = { itemRouter };    