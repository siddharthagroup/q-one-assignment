const { jwt } = require('../config')
const jwtToken = require("jsonwebtoken")


function authToken(user) {
  const expire = jwt.JWT_EXPIRE;
  const opt = expire ? { expiresIn: expire } : {};

  return jwtToken.sign(user, jwt.JWT_SECRET, opt);
}

module.exports = { authToken };
