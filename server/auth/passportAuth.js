const { checkUser } = require('../services/userService')
const { jwt } = require('../config')
const ObjectId = require("mongodb").ObjectId;
var JwtStrategy = require("passport-jwt").Strategy,
    ExtractJwt = require("passport-jwt").ExtractJwt,
    passport = require("passport");

var options = {
    jwtFromRequest: ExtractJwt.fromHeader("authorization"),
    secretOrKey: jwt.JWT_SECRET,
    passReqToCallback: true,
};

const jwtLogin = new JwtStrategy(options, async (req, payload, done) => {
    let user = await checkUser({ _id: new ObjectId(payload._id) });
    if (user) {
        return done(null, user);
    }
    const error = new Error("Please login");
    error.status = 401;
    return done(error, false);
});

passport.use(jwtLogin);

module.exports = {
    verifyAuth: passport.authenticate("jwt", { session: false }),
};
