const { dbClient } = require('../dbConfig')
const { ObjectId } = require('mongodb')

const Item = dbClient.model('Item');


// Create a new Item
exports.createItem = async (itemsInfo) => {
    if (Array.isArray(itemsInfo)) {
        return Item.insertMany(itemsInfo);
    }
    return Item.save(itemsInfo);
};


// Get Item By Id
exports.getItemById = async (params) => {
    return Item.findOne({ _id: new ObjectId(params.itemId) });
}

// Get Item By Name
exports.getItemByName = async (itemName) => {
    return Item.findOne({ itemName: itemName });
}


// Get all items
exports.getItemList = async () => Item.find({}).toArray();


// Update Item
exports.updateItem = async (query = null, update = null) => {
    if (!(query || update)) {
        return Promise.reject("Please provide valid params");
    }
    let updatedData = await Item.findOneAndUpdate(query, update);
    return updatedData;
};


// Delete Item By Id
exports.deleteItemById = async (params) => {
    return Item.findOneAndDelete({ _id: new ObjectId(params.itemId) });
};


// Check If Item Exists
exports.checkItem = async (query) => {
    let exist = await Item.findOne(query);
    return exist ? true : false;
};