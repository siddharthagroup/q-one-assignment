const { dbClient } = require('../dbConfig')


const User = dbClient.model('User');


exports.checkUser = async (params = {}) => {
    let userData = await User.findOne(params);
    return userData;
};

exports.createUser = async (userInfo) => {
    // for users array
    if (Array.isArray(userInfo)) {
        return User.insertMany(userInfo);
    }
    // for single user
    return User.save(userInfo);
};