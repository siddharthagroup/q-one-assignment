const { checkUser, createUser } = require('../services/userService')

exports.createInitialUsers = async () => {
  let users = await checkUser();
  if (users) {
    return;
  }
  let userArray = [
    {
      username: "test1",
      password: "123456",
    },
    {
      username: "test2",
      password: "123456",
    },
    {
      username: "test3",
      password: "123456",
    },
  ];

  createUser(userArray);
};
