const constants = {
  state: ["ACTIVE", "NOT_ACTIVE"],
  USER_CREATED: "Successfully created the User",
  USER_EXISTS: "User already exists",
  INVALID_PASS: 'Please provide username and password',
  INVALID_USER: 'Invalid User',
  ITEM_ALREADY_EXISTS: 'Item already exists',
  ITEM_ADDED: "Item Added Successfully",
  ITEM_FOUND: "Items Found Successfully",
  ITEM_UPDATED: "Items Updated Successfully",
  ITEM_DELETED: "Items Deleted Successfully",
  NO_ITEM_FOUND: 'No Item Found',
  DUPLICATE_ITEM: 'Duplicate Item Found',
  USER_VALIDATION: "Please provide valid username and password",
  ITEM_VALIDATION: "Please provide valid itemName and itemStatus",
  ERROR: "Error occured during execution"
};

module.exports = {
  constants,
};
