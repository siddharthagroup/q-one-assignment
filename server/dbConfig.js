const { MongoClient } = require("mongodb");
const { Modal } = require("./baseDao");
const config = require('./config')


class dbClient {
  constructor() { }

  database;
  model;

  static async connect() {
    const url = `mongodb+srv://${config.db.username}:${config.db.password}@cluster0.wxatls4.mongodb.net/?retryWrites=true&w=majority`
    const client = new MongoClient(url);

    // Connect to database
    await client.connect();

    this.database = client.db(config.db.dbName);
    console.log("Connected successfully to database");

    return this.database;
  }

  static model(collectionName) {
    let model = new Modal(collectionName, this.database);
    return model;
  }
}

module.exports = {
  dbClient
};
