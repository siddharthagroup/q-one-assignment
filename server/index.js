const { dbClient } = require("./dbConfig");


dbClient.connect().then(async () => {
    const express = require("express");
    const { router } = require("./routes");
    const { createInitialUsers } = require('./helpers/userHelper');


    createInitialUsers()

    const app = express();
    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));

    app.options('/*', (_, res) => {
        res.sendStatus(200);
    });

    app.use((request, response, next) => {
        response.header("Access-Control-Allow-Origin", "*");
        response.header('Access-Control-Allow-Credentials', true);
        response.header("Access-Control-Allow-Headers", "*");
        response.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
        next();
    });


    app.use("/", router);

    const port = 5001;

    /**
     * Initialize server
     */
    app.listen(port, () => {
        console.log(`Listening on port ${port}`);
    });

});