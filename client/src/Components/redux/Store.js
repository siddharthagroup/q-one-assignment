import { createStore, combineReducers } from 'redux';
import rootReducer from './reducers/index';

export const initialState = {
    activeStepKey:{activeStepKey:1},
    currItemID:''
}


const store = createStore(rootReducer, initialState);

export default store;