import { combineReducers } from 'redux';
import {ActiveKeyReducer, ChangeCurrItemID} from './reducer';

const rootReducer = combineReducers({
  activeStepKey:ActiveKeyReducer,
  currItemID:ChangeCurrItemID
});

export default rootReducer;