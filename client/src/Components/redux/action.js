export const changeActiveStepKey = (value) => ({
    type: 'CHANGEKEY',
    payload: value
});

export const changeCurrItemID = (value)=>({
  type: "CHANGEITEMID",
  payload:value
})
