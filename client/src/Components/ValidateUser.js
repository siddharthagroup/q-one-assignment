import React, { useState } from 'react';
import constants from '../Constants';
import { Formik, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import {
    FormStyled, 
    Label, 
    Input, 
    PrimaryButton, 
    ButtonHolder 
} from '../Components/StyledComponents'
import { changeActiveStepKey } from './redux/action';
import { connect } from 'react-redux';

const ValidateUser = ({activeStepKey,changeActiveStepKey}) => {

    const [responseData, setResponseData] = useState({});

    let initialValues = { userName: '', password: ''};

    const validationSchema = Yup.object({
        userName: Yup.string()
            .max(15, 'Must be 15 characters or less')
            .required('Required'),
        password: Yup.string()
            .max(20, 'Must be 20 characters or less')
            .required('Required'),
    });

    

    const submitHandler = async(values) => {
        try {
            const response = await fetch(`${constants.API_URL}/users/login`, {
            method: 'POST',
            body: JSON.stringify({
                username: values.userName,
                password: values.password
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8'
            }
            });
    
            if (!response.ok) {
            throw new Error('Request failed');
            }

            
            const data = await response.json();
            if(data.meta.Status){
                changeActiveStepKey(2);
                localStorage.setItem('authToken',data.Data);
                setResponseData(data);
            }else{
                window.alert(data.Data.err)
            }
           
        } catch (error) {
            console.error(error);
        }

        
    }


    return (
            <Formik
                initialValues={initialValues}
                validationSchema={validationSchema}
                onSubmit={submitHandler}
            >
                <FormStyled>
                    <Label htmlFor="userName">Enter User Name</Label>
                    <Input name="userName" type="text"  placeholder='Enter User Name'/>
                    <ErrorMessage name="userName" />

                    <Label htmlFor="password">Enter Password</Label>
                    <Input name="password" type="password" placeholder='Enter Password'/>
                    <ErrorMessage name="password" />
                    <ButtonHolder>
                    <PrimaryButton type="submit">Log In</PrimaryButton>
                    </ButtonHolder>
                </FormStyled>
            </Formik>
);
};

const mapStateToProps = (state) => ({
    activeStepKey:state.activeStepKey,
  });

  const mapDispatchToProps = {
    changeActiveStepKey
  };
  


export default connect(mapStateToProps, mapDispatchToProps)(ValidateUser)