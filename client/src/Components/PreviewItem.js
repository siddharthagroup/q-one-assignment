import React,{useEffect, useState} from 'react'
import constants from '../Constants';
import { changeActiveStepKey, changeCurrItemID } from './redux/action';
import { connect } from 'react-redux';
import {
  Previewrow,
  PreviewValue, 
  PreviewHeading, 
  PrimaryButton, 
  SecondaryButton, 
  PreviewContainer, 
  ButtonHolder
} from './StyledComponents'

const PreviewItem = ({currItemID, changeActiveStepKey, changeCurrItemID}) => {


  const [responseData, setResponseData] = useState({});

  useEffect(()=>{
    getLastItemAdded();
  },[])

  const getLastItemAdded = async() => {
    try {
        const response = await fetch(`${constants.API_URL}/items/getItemById?itemId=${currItemID.currItemID}`,{
          method: 'GET',
          headers:{
            'Content-type': 'application/json; charset=UTF-8',
            'authorization' : localStorage.getItem('authToken')
        }
          });

        if (!response.ok) {
        throw new Error('Request failed');
        }

        const data = await response.json();
        setResponseData(data.Data);
    } catch (error) {
        console.error(error);
    }
  }

  const AddMoreItem= ()=>{
    
    changeActiveStepKey(2);
  }

  const submit = ()=>{
    changeActiveStepKey(4);
  }

  return (
    <PreviewContainer>
      <div>
        <Previewrow>
          <PreviewHeading>Item Name</PreviewHeading>
          <PreviewValue>{responseData.itemName}</PreviewValue>
        </Previewrow>
        <Previewrow>
          <PreviewHeading>User Name</PreviewHeading>
          <PreviewValue>{responseData.addedBy}</PreviewValue>
        </Previewrow>
        <Previewrow>
          <PreviewHeading>Date</PreviewHeading>
          <PreviewValue>{responseData.dateAdded}</PreviewValue>
        </Previewrow>
        <Previewrow>
          <PreviewHeading>State</PreviewHeading>
          <PreviewValue>{responseData.itemStatus}</PreviewValue>
        </Previewrow>
        
      </div>
      <ButtonHolder>
        <SecondaryButton onClick={()=>{AddMoreItem()}}>Add more Item</SecondaryButton>
        <PrimaryButton type="submit" from={'previewItem'} onClick={()=>{submit()}}>Submit</PrimaryButton>
      </ButtonHolder>
      
    </PreviewContainer>
  )
}
const mapStateToProps = (state) => ({
  activeStepKey:state.activeStepKey,
  currItemID: state.currItemID
});

const mapDispatchToProps = {
  changeActiveStepKey,
  changeCurrItemID
};

export default connect(mapStateToProps, mapDispatchToProps)(PreviewItem)
