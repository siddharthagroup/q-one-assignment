import React, { useState, useEffect } from 'react'
import constants from '../Constants';
import {
  TableContainer,
  Table, 
  Action, 
  TableData,
  TableRow, 
  TableHeader,
  TableBody,
  PrimaryButton, 
  SecondaryButton, 
  ButtonHolder, 
  EditButton, 
  DeleteButton  
} from './StyledComponents'
import { changeActiveStepKey } from './redux/action';
import { connect } from 'react-redux';
import PopUp from './PopUp';


function EditItem({activeStepKey,changeActiveStepKey}) {
    const [isOpen, setIsOpen] = useState(false)
    const [modalType, setModalType] = useState('')
    const [itemData, setItemData] = useState({});
    const [responseData, setResponseData] = useState([]);

  useEffect(()=>{

      getItems();
    

  },[isOpen])

  const getItems = async() => {
    try {
        const response = await fetch(`${constants.API_URL}/items/getItemList`,{
          method: 'GET',
          headers:{
            'Content-type': 'application/json; charset=UTF-8',
            'authorization' : localStorage.getItem('authToken')
          }
          });

        if (!response.ok) {
        throw new Error('Request failed');
        }

        const data = await response.json();
        setResponseData(data.Data);
    } catch (error) {
        console.error(error);
    }
  }

    const logout = () => {
      localStorage.clear();
      changeActiveStepKey(1);
    }
    const handleAddMore = () => {
      changeActiveStepKey(2);
    }
    const handleEdit = (itemObj) => {
      setItemData(itemObj);
      setIsOpen(true);
      setModalType('EDIT')
    }
    const handleDelete = (itemData) => {
      setIsOpen(true);
      setItemData(itemData)
      setModalType('DELETE')
    }
    return (
        <TableContainer>
            <Table>
              <TableBody>
                
                <TableRow>
                    <TableHeader>Item Name</TableHeader>
                    <TableHeader>User Added</TableHeader>
                    <TableHeader>Date Added</TableHeader>
                    <TableHeader>State</TableHeader>
                    <TableHeader>Action</TableHeader>


                </TableRow>
                {responseData.map((item,idx)=>{
                  return (
                    <TableRow key={idx}>
                    <TableData>{item.itemName}</TableData>
                    <TableData>{item.addedBy}</TableData>
                    <TableData>{item.dateAdded}</TableData>
                    <TableData>{item.itemStatus}</TableData>
                    <TableData>
                      <Action>
                        <EditButton onClick={()=>handleEdit(item)}>
                          Edit
                        </EditButton>
                        <DeleteButton onClick={()=>handleDelete(item)}>
                          Delete
                        </DeleteButton>
                      </Action>
                    </TableData>
                  </TableRow>
                  )
                })}

              </TableBody>
            </Table>
            <ButtonHolder>
                <SecondaryButton onClick={handleAddMore}>Add More Items</SecondaryButton>    
                <PrimaryButton onClick={logout}>Log Out</PrimaryButton>
            </ButtonHolder>
            { isOpen ?  <PopUp type={modalType} setIsOpen={setIsOpen} itemData={itemData}/>: null}
        </TableContainer>

    )
}

const mapStateToProps = (state) => ({
  activeStepKey:state.activeStepKey,
});

const mapDispatchToProps = {
  changeActiveStepKey
};

export default connect(mapStateToProps, mapDispatchToProps)(EditItem)