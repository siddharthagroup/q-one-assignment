import styled from "styled-components";
import { Field, Formik, useFormik, Form } from "formik";

const bgColor = '#ff8b54';
const dngColor = '#a23535';
export const FormikStyled = styled(Formik)`
  display: flex;
  flex-direction: column;
  padding: 20px;
  background-color: ${bgColor};
  border-radius: 5px;
  width: 70%;
`;
export const FormStyled = styled(Form)`
display: flex;
flex-direction: column;
width: 70%;
justify-content:center;
`;

export const Label = styled.label`
  display: flex;
  margin-bottom: 10px;
  margin-top: 10px;
  font-weight: 600;
  // opacity: 0.5;
  color: ${(props) => props.color ?? "grey"};
`;

export const Input = styled(Field)`
  margin-bottom: 10px;
  padding: 10px;
  border: solid #989898;
  border-radius: 8px;
  opacity: 0.5;
  font-size: 14px;
`;
export const Select = styled.select`
  margin-bottom: 10px;
  padding: 10px;
  border: solid #989898;
  border-radius: 8px;
  opacity: 0.5;
  font-size: 14px;
`;
export const Option = styled.option`
  margin-bottom: 10px;
  padding: 10px;
  border: solid #989898;
  border-radius: 8px;
  opacity: 0.5;
  font-size: 14px;
`;

export const ErrorMessage = styled.span`
  color: red;
`;

export const StepContainer = styled.div`
    display: flex;
    justify-content: space-evenly;
    align-items: center;
    
`;

export const Step = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 25%;
    height: 100px;
    border: 1px solid ${bgColor};
    color: ${bgColor};
    background-color : white;
`;

export const ActiveStep = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 25%;
    height: 100px;
    color: white;
    background-color : ${bgColor};
    border: 1px solid ${bgColor};
`;

export const StepHeading = styled.h3`
//   color: ${bgColor};
height: 10px;

`;

export const StepContent = styled.span`
//   color: ${bgColor};

`;

export const FormContainer = styled.div`
    
width: 100%;
border: 1px solid ${bgColor};
min-height: 770px;
display: flex;
justify-content: center;

`;

export const PrimaryButton = styled.button`
background-color: ${props => props.danger ? dngColor : bgColor};
color: white;
width: 120px;
height: 40px;
border: none;
font-weight: 900;
font-size: 15px;
border-radius: 8px;
cursor:pointer;
`;


export const PreviewContainer = styled.div`
display: flex;
flex-direction: column;
width: 32%;
justify-content: center;
align-items: center;
`;
export const Previewrow = styled.div`
display: flex;
align-items: center;
gap: 50px;
// justify-content: space-between;
`;

export const PreviewHeading = styled.h4`
width: 100px;
display: flex;
`;
export const PreviewValue = styled.span`
width: 100px;
    display: flex;
`;
export const SecondaryButton = styled.button`
background-color: white;
color: ${props => props.danger ? dngColor : bgColor};
height: 40px;
border: 1px solid ${props => props.danger ? dngColor : bgColor};
font-weight: 900;
font-size: 15px;
border-radius: 8px;
cursor:pointer;
padding: 10px;
min-width: 120px;
`;
export const ButtonHolder = styled.div`
display: flex;
justify-content: center;
gap: 70px;

`;

export const TableContainer = styled.th`
width: 100%;
display: flex;
flex-direction: column;
gap: 40px;
`;
export const TableBody = styled.tbody`

`;
export const TableHeader = styled.th`
border: 1px solid ${bgColor};
color:grey;
text-align: left;
padding-left: 20px;
height:50px;
`;
export const TableData = styled.td`
border: 1px solid ${bgColor};
text-align: left;
color:grey;
padding-left: 20px;
height:50px;
font-weight: 400;
`;
export const Table = styled.table`
border-collapse: collapse;
width: 100%;
`;
export const Action = styled.td`
cursor: pointer;
display: flex;
float: left;
justify-content: space-evenly;
align-items: center;
gap: 20px;
`;
export const TableRow = styled.tr`
// height:50px;
`;
export const EditButton = styled.span`
    color: blue;
    border-bottom: 2px solid blue;
`;
export const DeleteButton = styled.span`
    color: red;
    border-bottom: 2px solid red;
`;
export const Modal = styled.div`
// height: 200px;
background: white;
width: ${props => props.width}%;
border: 1px solid #80808045;
border-radius: 4px;
filter: drop-shadow(2px 1px 3px grey);
position: absolute;
left: ${props => props.left ? props.left : 15}%;
top: 20%;
`;
export const ModalContainer = styled.div`
height: 500px;
width: 100%;
background: #eaeaea85;
position: absolute;
`;

export const ModalHeader = styled.div`
    height: 40px;
    background: ${props => props.color ? props.color : bgColor};
    color: white;
    display: flex;
    align-items: center;
    left: 20px;
    padding-left: 30px;
    padding-right: 20px;
    border-radius: 4px 4px 0px 0px;
    justify-content: space-between;
`;
export const ModalContent = styled.div`
display: flex;
justify-content: center;
align-items: center;
padding: 20px;
`;
export const CloseButton = styled.div`
height:20px;
width:20px;
cursor:pointer;
`;
export const AppContainer = styled.div`

`;












