import React from 'react'
import ValidateUser from './ValidateUser'
import {
    StepContainer, 
    Step, 
    FormContainer, 
    StepHeading, 
    StepContent,
    ActiveStep,
    AppContainer
} from './StyledComponents'
import AddItem from './AddItem';
import EditItem from './EditItem';
import PreviewItem from './PreviewItem';
import { connect } from 'react-redux';

const Home = ({activeStepKey}) => {

    const activeKey = activeStepKey.activeStepKey
    

    let initialSteps = [
        {
            heading: 'Step1',
            content:'Log In'
        },
        {
            heading: 'Step2',
            content:'Add item'
        },
        {
            heading: 'Step3',
            content:'Preview item'
        },
        {
            heading: 'Step4',
            content:'Edit item'
        }

    ];



    const returnActiveTab = ()=>{

        if(activeKey === 1){
            return (
                <ValidateUser />
            )
        }else if(activeKey === 2){
            return(
                <AddItem/>
            )
        }else if(activeKey === 3){
            return(
                <PreviewItem/>
            )
        }

        return(
            <EditItem/>
        )
    }

  return (
    <AppContainer>
        <StepContainer>
            {initialSteps.map((item, idx)=>{
                if(idx < activeKey){
                    return(
                    <ActiveStep key={idx}>
                        <StepHeading>{item.heading}</StepHeading>
                        <StepContent>{item.content}</StepContent>
                    </ActiveStep>

                    )
                }else{
                    return(
                        <Step key={idx}>
                            <StepHeading>{item.heading}</StepHeading>
                            <StepContent>{item.content}</StepContent>
                        </Step>
    
                        )
                }
            })}
        </StepContainer>
        
        {/* get active tab form  */}  
        <FormContainer>
            {returnActiveTab()}  
        </FormContainer>
    </AppContainer>
  )
}

const mapStateToProps = (state) => ({
    activeStepKey:state.activeStepKey,

  });
  


export default connect(mapStateToProps)(Home)
