import React,{useState} from 'react'
import constants from '../Constants';
import { Formik, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import {
  FormStyled, 
  Label, 
  Input, 
  PrimaryButton, 
  Select, 
  Option, 
  ButtonHolder 
} from '../Components/StyledComponents'
import { changeActiveStepKey, changeCurrItemID } from './redux/action';
import { connect } from 'react-redux';

const AddItem = ({changeActiveStepKey, changeCurrItemID}) => {

  const [responseData, setResponseData] = useState({});


  const handleAddItem = async(values) => {
    console.log(localStorage.getItem('authToken'),"add");
    try {
        const response = await fetch(`${constants.API_URL}/items/addItem`, {
        method: 'POST',
        body: JSON.stringify({
          itemName: values.itemName,
          itemStatus: values.itemState
        }),
        headers:{
          'Content-type': 'application/json; charset=UTF-8',
          'authorization' : localStorage.getItem('authToken')
        }
        });

        console.log(constants.Headers,"vfdhvd")

        if (!response.ok) {
        throw new Error('Request failed');
        }
       
        const data = await response.json();
        
        if(data.meta.Status){
          changeCurrItemID(data.Data.insertedId);
          changeActiveStepKey(3);
          setResponseData(data);
        }else{
          window.alert(data.Data.err)
        }
    } catch (error) {
        console.error(error);
    }


}

  return (
    <Formik
        initialValues={{ itemName: "", itemState: '' }}
        onSubmit={ values => {
          handleAddItem(values)
        }}
        validationSchema={Yup.object().shape({
          itemName: Yup.string()
            .required("field required"),
          itemState: Yup.string()
            .required("State required"),
        })}
      >
        {props => {
          const {
            values,
            handleChange,
            handleBlur,
            handleSubmit,
          } = props;
          return (
            <FormStyled onSubmit={handleSubmit}>

                <Label htmlFor="itemName">Enter Name</Label>
                <Input name="itemName" type="text"  placeholder='Enter Name'/>
                <ErrorMessage name="itemName" />

                <Label htmlFor="itemState">Select State</Label>
                <Select as="select" name="itemState"
                  value={values.itemState}
                  onChange={handleChange}
                  onBlur={handleBlur}
                 >
                    <Option value="">Select State</Option>
                    <Option value="ACTIVE">Active</Option>
                    <Option value="NOT_ACTIVE">Not Active</Option>
                </Select>
                <ErrorMessage name="itemState" />
                <ButtonHolder>
                    <PrimaryButton  type="submit" >Add Item</PrimaryButton>
                </ButtonHolder>
            </FormStyled>
          );
        }}
      </Formik>
  )
}

const mapStateToProps = (state) => ({
    activeStepKey : state.activeStepKey,
    currItemID : state.currItemID
  });

  const mapDispatchToProps = {
    changeActiveStepKey,
    changeCurrItemID
  };
  
export default connect(mapStateToProps, mapDispatchToProps)(AddItem)
