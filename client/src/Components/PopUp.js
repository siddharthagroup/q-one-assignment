import React,{useState} from 'react'
import constants from '../Constants';
import { 
  Modal, 
  ModalContainer, 
  ModalHeader, 
  ModalContent, 
  FormStyled, 
  Label, 
  Input, 
  PrimaryButton, 
  Select, 
  Option,
  ButtonHolder, 
  SecondaryButton 
} from './StyledComponents'
import { Formik, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { changeActiveStepKey } from './redux/action';

const PopUp = ({itemData, type, setIsOpen}) => {

  const [responseData, setResponseData] = useState({});

  const deleteItem = async(event, itemData)=>{
    try {
      const response = await fetch(`${constants.API_URL}/items/deleteItemById/${itemData._id}`, {
        method: 'DELETE',
        headers:{
          'Content-type': 'application/json; charset=UTF-8',
          'authorization' : localStorage.getItem('authToken')
        }
        });


        const data = await response.json();
        setIsOpen(false);
        setResponseData(data);
    } catch (error) {
        console.error(error);
    }

  }

  const handleAddItem = async(values) => {

    try {
      const response = await fetch(`${constants.API_URL}/items/updateItemById/${itemData._id}`, {
        method: 'PUT',
        body: JSON.stringify({
          itemName: values.itemName,
          itemStatus: values.itemState
        }),
        headers:{
          'Content-type': 'application/json; charset=UTF-8',
          'authorization' : localStorage.getItem('authToken')
        }
        });


        const data = await response.json();
        setIsOpen(false);
        setResponseData(data);
    } catch (error) {
        console.error(error);
    }


}

  const toggleModal =(e)=>{
      e.stopPropagation();
     setIsOpen(false)
  }

  const returnModal=()=>{
    if(type === 'EDIT'){
      return(
        <Modal width={70} >
        <ModalHeader>
         Edit Item
       </ModalHeader>
         <ModalContent>
         <Formik
        initialValues={{ itemName: itemData.itemName, itemState: itemData.itemStatus }}
        onSubmit={ values => {
          handleAddItem(values);
        }}
        validationSchema={Yup.object().shape({
          itemName: Yup.string()
            .required("field required"),
          itemState: Yup.string()
            .required("State required"),
        })}
      >
        {props => {
          const {
            values,
            handleChange,
            handleBlur,
            handleSubmit,
          } = props;
          return (
            <FormStyled onSubmit={handleSubmit}>

                <Label htmlFor="itemName">Enter Name</Label>
                <Input name="itemName" type="text"  placeholder='Enter Name'/>
                <ErrorMessage name="itemName" />

                <Label htmlFor="itemState">Enter State</Label>
                <Select as="select" name="itemState"
                  value={values.itemState}
                  onChange={handleChange}
                  onBlur={handleBlur}
                 >
                    <Option value="">Select State</Option>
                    <Option value="ACTIVE">Active</Option>
                    <Option value="NOT_ACTIVE">Not Active</Option>
                </Select>
                <ErrorMessage name="itemState" />
                <ButtonHolder style={{justifyContent: 'flex-end'}}>
                    <SecondaryButton  type="submit" onClick={(e)=>toggleModal(e)}>Cancel</SecondaryButton>
                    <PrimaryButton  type="submit" >Save</PrimaryButton>
                </ButtonHolder>
            </FormStyled>
          );
        }}
      </Formik>
         </ModalContent>
       </Modal>
      )
    }else{
      return(
        <Modal width={30} left={30}>
        <ModalHeader color={'#a23535'}>
         Warning
       </ModalHeader>
         <ModalContent style={{gap: '30px', flexDirection: 'column', color: '#a23535'}}>
          Are you sure ?
         <ButtonHolder style={{justifyContent: 'flex-end'}}>
                    <SecondaryButton danger="false" type="submit" onClick={(e)=>toggleModal(e)}>No, keep</SecondaryButton>
                    <PrimaryButton danger="true" type="submit" onClick={(e)=>deleteItem(e, itemData)}>Yes, delete</PrimaryButton>
                </ButtonHolder>
         </ModalContent>
       </Modal>
      )
    }

   
  }

  return (
    <ModalContainer >
     
       {returnModal()}

    </ModalContainer>
   
  )
}

export default PopUp
