import ValidateUser from './Components/ValidateUser'
import './App.css';
import Home from './Components/Home';
import { Provider } from 'react-redux';
import store from './Components/redux/Store';


function App() {
  return (

    <Provider store={store}>
      <Home />
    </Provider>
  );
}

export default App;
