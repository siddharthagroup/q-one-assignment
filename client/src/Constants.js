
const constants={
    
    API_URL:'http://localhost:5001',
    Headers:{
        'Content-type': 'application/json; charset=UTF-8',
        'authorization' : localStorage.getItem('authToken')
    }
}
export default constants;