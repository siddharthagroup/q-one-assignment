# Q-One Tech Assignment

## Project Start

Install current stable version of nodejs

## Install Packages for both client and server
 command - npm install


## Backend Technologies

• NodeJS
• ExpressJS
• MongoDB
• Node input validator
• Passport JS with JWT (auth)
• MongoDB Atlas for testing


## Frontend Technologies

• ReactJS
• Redux
• Styled components (custom CSS)
• Yup
• Formik


## Server

base url api -
   http://localhost:5001/

project url -
   http://localhost:3000/

## Test User

username - test1
password - 123456